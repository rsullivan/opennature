from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_retrieve_dois(self):
        # User wants to display a list of Nature's CC articles so goes to homepage
        self.browser.get('http://localhost:8000')
        # User notices the page title
        self.assertIn('Nature Creative Commons', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1')
        self.assertIn('Nature Creative Commons', header_text)
       #User is invited browse creative commons articles available on Nature (using an input box for now)
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'), 'Enter a subject term')
       #User enters genetics as a subject term
        inputbox.send_keys('Genetics')
        inputbox.send_keys(Keys.ENTER)

        table = self.browser.find_element_by_id('id_subject_table')
        rows = table.find_elements_by_tag_name('tr')
        any(row.text == '1: Genetics' for row in rows)
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main()
